/*
Learn Go with Tests solved this with a Switch, which runs faster
 - Before reading their answer I solved with a map, leaving as greetingPrefixMap()
*/
package main

import (
	"fmt"
	"strings"
)

const (
	french             = "French"
	spanish            = "Spanish"
	englishHelloPrefix = "Hello, "
	spanishHelloPrefix = "Hola, "
	frenchHelloPrefix  = "Bonjour, "
)

var (
	supportedLanguages = map[string]string{
		"english": "Hello, ",
		"spanish": "Hola, ",
		"french":  "Bonjour, ",
	}
)

func Hello(name string, language string) string {
	if name == "" {
		name = "World"
	}

	return greetingPrefixMap(language) + name
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case french:
		prefix = frenchHelloPrefix
	case spanish:
		prefix = spanishHelloPrefix
	default:
		prefix = englishHelloPrefix
	}
	return
}

func greetingPrefixMap(language string) (prefix string) {
	// "comma ok" idiom, HelloPrefix=map[key], ok=true/false for key exists/doesn't
	// So if the map key "language" exists, return the corresponding map value (+ name)
	if HelloPrefix, ok := supportedLanguages[strings.ToLower(language)]; ok {
		prefix = HelloPrefix
	} else {
		prefix = supportedLanguages["english"]
	}
	return
}

func main() {
	fmt.Printf(Hello("Test", ""))
}
