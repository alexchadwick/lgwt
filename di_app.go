package main

import (
	"os"

	di "gitlab.com/alexchadwick/lgwt/dependencyInjection"
)

func main() {
	di.Greet(os.Stdout, "Elodie")
}
