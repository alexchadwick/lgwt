package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const (
	finalWord      = "Go!"
	countdownStart = 3
)

type ConfigurableSleeper struct {
	duration time.Duration
	sleep    func(time.Duration)
}

// Use ConfigurableSleeper.sleep which meets req for Sleeper interface, so tests can mock with SpyTime.Sleep()
func (c ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

// Used by both real and SpySpleer
type Sleeper interface {
	Sleep()
}

func Countdown(out io.Writer, sleeper Sleeper) {
	for i := countdownStart; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(out, i) // Fprintln() will accept int `i` and add a \n, unlike Fprint
	}

	sleeper.Sleep()
	fmt.Fprint(out, finalWord)
}

func main() {
	sleeper := &ConfigurableSleeper{1 * time.Second, time.Sleep}
	Countdown(os.Stdout, sleeper)
}
