package main

func Sum(numbers []int) int {
	var sum int
	// Ignore first returned value, index of array
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func SumAll(numbers ...[]int) (sums []int) {
	// For each slice submitted
	for _, i := range numbers {
		sums = append(sums, Sum(i))
	}
	return
}

// SumAll ignoring the first value
func SumAllTails(numbers ...[]int) (sums []int) {
	for _, i := range numbers {
		if len(i) > 1 {
			i = i[1:]
			sums = append(sums, Sum(i))
		} else {
			sums = append(sums, 0)
		}
	}
	return
}
