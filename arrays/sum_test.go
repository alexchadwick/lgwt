package main

import (
	"testing"
)

func TestSum(t *testing.T) {
	t.Run("slice of 5 numbers", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5}
		got := Sum(numbers)
		want := 15

		if got != want {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})
}

func TestSumAll(t *testing.T) {
	assertSlicesEqual(
		t,
		[]int{3, 9},
		SumAll([]int{1, 2}, []int{0, 9}),
	)
}

// Sum of []int, omitting the first value (the head)
func TestSumAllTails(t *testing.T) {

	t.Run("2 slices, 2+ values", func(t *testing.T) {
		got := SumAllTails([]int{1, 2}, []int{0, 9, 7})
		want := []int{2, 16}

		assertSlicesEqual(t, want, got)
	})

	t.Run("Empty slices", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{})
		want := []int{0, 0}

		assertSlicesEqual(t, want, got)
	})

	t.Run("Single value slice", func(t *testing.T) {
		got := SumAllTails([]int{1}, []int{0, 9})
		want := []int{0, 9}

		assertSlicesEqual(t, want, got)
	})
}

// Loop over want slice and compare to same index on got slice
func assertSlicesEqual(t *testing.T, want, got []int) {
	t.Helper()
	for i, w := range want {
		if w != got[i] {
			t.Errorf("got %d want %d", got[i], w)
		}
	}
}
