package structs

import "testing"

func TestPerimeter(t *testing.T) {
	rectangle := Rectangle{10.0, 10.0}
	got := Perimeter(rectangle)
	want := 40.0

	if got != want {
		t.Errorf("got %.2f want %.2f", got, want)
	}
}

// Uses table driven test style
func TestArea(t *testing.T) {

	// `anonymous struct`: declare slice of structs, w/ 2 fields (shape, want)
	// then fill slices with cases
	areaTests := []struct {
		name  string
		shape Shape
		want  float64
	}{
		{shape: Rectangle{Width: 12, Height: 6}, want: 72.0},
		{shape: Circle{Radius: 10}, want: 314.1592653589793},
		{shape: Triangle{Base: 12, Height: 6}, want: 36.0},
	}

	// Loop over test table outer structs
	for _, tt := range areaTests {
		// `Shape` interface method `Area` resolves to same in structs `Rectangle`,`Circle` & `Triangle`
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Area()
			if got != tt.want {
				t.Errorf("%#v got %g want %g", tt.shape, got, tt.want)
			}
		})
	}
}

// Replaced by TestArea, which uses table driven test strategy
//func TestAreaNotTable(t *testing.T) {
//
//	checkArea := func(t testing.TB, shape Shape, want float64) {
//		t.Helper()
//		got := shape.Area()
//		if got != want {
//			t.Errorf("got %g want %g", got, want)
//		}
//	}
//
//	t.Run("rectangles", func(t *testing.T) {
//		rectangle := Rectangle{12, 6}
//		checkArea(t, rectangle, 72.0)
//	})
//
//	t.Run("circles", func(t *testing.T) {
//		circle := Circle{10}
//		checkArea(t, circle, 314.1592653589793)
//	})
//
//}

// Replaced by TestAreaNotTable(), which uses interface `shape` to allow passing both Rectangle
// and Cirle structs
//func TestAreaOld(t *testing.T) {
//
//	t.Run("rectangle", func(t *testing.T) {
//		rectangle := Rectangle{5.0, 10.0}
//		got := rectangle.Area()
//		want := 50.0
//
//		if got != want {
//			t.Errorf("got %g want %g", got, want)
//		}
//	})
//
//	t.Run("circle", func(t *testing.T) {
//		circle := Circle{10}
//		got := circle.Area()
//		want := 314.1592653589793
//
//		if got != want {
//			t.Errorf("got %g want %g", got, want)
//		}
//	})
//}
