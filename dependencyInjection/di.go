package di

import (
	"fmt"
	"io"
)

// Using io.Writer allows us to pass methods like os.Stdout (output to a user) or bytes.Buffer{} (used in testing)
func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}
