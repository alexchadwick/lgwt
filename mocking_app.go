package main

import (
	"os"
	"time"

	"gitlab.com/alexchadwick/lgwt/mocking"
)

func main() {
	sleeper := &mocking.ConfigurableSleeper{1 * time.Second, time.Sleep}
	mocking.Countdown(os.Stdout, sleeper)
}
