package iteration

import (
	"fmt"
	"strings"
	"testing"
)

const (
	initial     = "a"
	repeatCount = 5
)

func TestRepeat(t *testing.T) {
	got := Repeat(initial, repeatCount)
	want := strings.Repeat(initial, repeatCount)

	if got != want {
		t.Errorf("expected '%q' but got '%q'", want, got)
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat(initial, repeatCount)
	}
}

// Example description goes here
func ExampleRepeat() {
	fmt.Println(Repeat(initial, repeatCount))
	// Output: aaaaa
}
